#include <gtk/gtk.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <string>
#include <cstring>
#include <ctime>

GtkWidget *window,*grid,*title,*label,*entry,*restartBtn;

int n1 = 0;
int n2 = 0;
time_t t;
int score = 0;

static void destroy(GtkWidget *widget,gpointer   data)
{
    gtk_main_quit ();
}

static std::string createEquation()
{
	n1 = rand() % 10;
	n2 = rand() % 10;
	return std::to_string(n1) + " + " + std::to_string(n2);
}

static void endScreen()
{
	gtk_label_set_text((GtkLabel*)label,("Times up! Score: "+std::to_string(score)).c_str());
	gtk_widget_hide(entry);
	gtk_widget_hide(title);
	gtk_widget_show(restartBtn);
}

static void check()
{
	if(60 - (time(0) - t) <= 0)
	{
		endScreen();
	}
	const char* str = gtk_entry_get_text((GtkEntry*)entry);
	if(std::strlen(str) > 0)
	{
		if(n1 + n2 == std::stoi(str))
		{
			gtk_entry_set_text((GtkEntry*)entry,"");
			gtk_label_set_text((GtkLabel*)label,createEquation().c_str());
			score++;
		}
	}
}

static void restart()
{
	t = time(0);
	score = 0;
	gtk_widget_hide(restartBtn);
	gtk_entry_set_text((GtkEntry*)entry,"");
	gtk_widget_show(entry);
	gtk_widget_show(title);
	gtk_label_set_text((GtkLabel*)label,createEquation().c_str());
}

int main (int argc, char *argv[])
{
	srand (time(NULL));
  t = time(0);
  gtk_init(&argc, &argv);
  void initialize_window(GtkWidget *);

  //Create the main window
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  initialize_window(window);

  // Create Grid
  grid = gtk_grid_new();
  gtk_grid_insert_row((GtkGrid*)grid,0);
  gtk_grid_insert_row((GtkGrid*)grid,1);
  gtk_grid_insert_row((GtkGrid*)grid,2);
  gtk_grid_insert_row((GtkGrid*)grid,3);
  gtk_grid_insert_column((GtkGrid*)grid,0);
  
  // Create and attach Title
  title = gtk_label_new("Answer the below question:");
  gtk_grid_attach((GtkGrid*)grid,title,0,0,1,1);
  
  // Create and attach label
  label = gtk_label_new(createEquation().c_str());
  gtk_grid_attach((GtkGrid*)grid,label,0,1,1,1);

  // Create and attach entry
  entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(entry), "");
  gtk_grid_attach (GTK_GRID (grid), entry, 0, 2, 1, 1);
  g_signal_connect(entry,"activate",G_CALLBACK(check),NULL);
  
  // Create and attach restart button
  restartBtn = gtk_button_new_with_label("Restart");
  gtk_grid_attach (GTK_GRID (grid), restartBtn, 0, 3, 1, 1);
  g_signal_connect(restartBtn,"pressed",G_CALLBACK(restart),NULL);

  gtk_container_add(GTK_CONTAINER(window),grid);
	
  gtk_widget_show_all(window);
  gtk_widget_hide(restartBtn);
    
  gtk_main ();
  	
  return 0;
}



void initialize_window(GtkWidget *window)
{
  gtk_window_set_title(GTK_WINDOW(window),"My Window"); //Set window title
  gtk_window_set_default_size (GTK_WINDOW (window), 100, 100); //Set default size for the window
  g_signal_connect (window, "destroy",G_CALLBACK (destroy), NULL); //End application when close button clicked

}
