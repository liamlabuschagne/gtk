#include <gtk/gtk.h>
#include <iostream>
 
GtkWidget *window1;
GtkWidget *button1;

class Obj {
private:
	int age;
	char** name;
public:
	Obj(int age, char** name){
		this.age = age;
		this.name = name;
	}
}
 
int main(int argc, char *argv[])
{
    GtkBuilder* builder; 
    GtkWidget* window;
 
    gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "UI.glade", NULL);
 
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    gtk_builder_connect_signals(builder, NULL);
    
    button1 = GTK_WIDGET(gtk_builder_get_object(builder, "button1"));
 
    g_object_unref(builder);
 
    gtk_widget_show(window);        
    gtk_main();
	
	Obj o = new Obj(5,"Liam");
	std::cout << o.age << o.name;
    return 0;
}
