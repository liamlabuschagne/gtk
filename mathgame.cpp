#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

static void destroy(GtkWidget *widget,gpointer   data)
{
    gtk_main_quit ();
}

int main (int argc, char *argv[])
{
  GtkWidget *window,*grid,*label,*entry;
  gtk_init(&argc, &argv);
  void initialize_window(GtkWidget *);

  //Create the main window
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  initialize_window(window);
  gtk_widget_show(window);

  // Create Grid
  grid = gtk_grid_new();
  gtk_grid_insert_row(grid,0);
  gtk_grid_insert_column(grid,0);
  
  // Create and attach label
  label = gtk_label_new("Label");
  gtk_grid_attach(grid,label,0,0,1,1);
  
  
  gtk_main ();
  return 0;
}



void initialize_window(GtkWidget *window)
{
  gtk_window_set_title(GTK_WINDOW(window),"My Window"); //Set window title
  gtk_window_set_default_size (GTK_WINDOW (window), 400, 200); //Set default size for the window
  g_signal_connect (window, "destroy",G_CALLBACK (destroy), NULL); //End application when close button clicked

}
