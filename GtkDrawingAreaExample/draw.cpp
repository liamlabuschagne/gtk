#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

int MAZE_WIDTH = 10;
int MAZE_HEIGHT = 10;
/*
1 1 0 1 1 1 1 1 1 1
0 1 0 0 0 0 0 0 0 1
0 1 1 1 1 1 1 1 0 1
0 0 0 0 0 0 0 0 0 1
1 1 1 1 1 0 1 1 1 1
*/





bool maze[100] = {1,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,1,1,0,1};

int xpos = 9;
int ypos = 10;

gboolean draw_callback(GtkWidget *widget,cairo_t *cr,gpointer data)
{
	guint width,height;
	GdkRGBA color; // 
	GtkStyleContext *context; // Refrence to the drawing area object
	
	// Set up the drawing area
	context=gtk_widget_get_style_context(widget);
	gtk_render_background(context,cr,0,0,MAZE_WIDTH*10,MAZE_HEIGHT*10);
	
	// Draw our grid
	for(int y = 0; y < MAZE_HEIGHT; y++)
	{
		for(int x = 0; x < MAZE_WIDTH; x++)
		{
			if(maze[y*MAZE_WIDTH+x]){
				cairo_rectangle(cr,x*10,y*10,10,10);
			}
		}
	}
	
	cairo_fill(cr);
	
	gdk_rgba_parse(&color,"rgb(0,100,0)");
	gdk_cairo_set_source_rgba(cr,&color);
	
	cairo_rectangle(cr,(xpos*10)-10,(ypos*10)-10,10,10);
	
	cairo_fill (cr);
	
	return FALSE;
}

gboolean on_key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	const char* key = gdk_keyval_name(event->keyval);
	ypos--;
	if(key == "Up"){
		// Move up
		ypos--;
	}
	else if(key == "Down"){
		// Move down
	} 
	else if(key == "Left"){
		// Move left
	}
	else if(key == "Right"){
		// Move right
	}
	else {
		printf("Does not match any of theses.\n");
	}
	return FALSE; 
}

gint main(int argc,char *argv[])
{
	GtkWidget *window,*drawing_area;
	
	// Init window
	gtk_init(&argc,&argv);
	window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	// Connect events
	g_signal_connect(window,"destroy",G_CALLBACK(gtk_main_quit),NULL);
	g_signal_connect (G_OBJECT (window), "key_press_event", G_CALLBACK (on_key_press), NULL);
	
	// Create drawing area
	drawing_area=gtk_drawing_area_new();
	gtk_container_add (GTK_CONTAINER (window),drawing_area);
	
	// Init drawing area
	gtk_widget_set_size_request(drawing_area,MAZE_WIDTH*10,MAZE_HEIGHT*10);
	g_signal_connect(G_OBJECT(drawing_area),"draw", G_CALLBACK(draw_callback),NULL);
	
	// Start the app
	gtk_widget_show_all(window);
	gtk_main();
	return 0;
}
